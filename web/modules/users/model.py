from flask.ext.login import UserMixin


class User(UserMixin):
    def __init__(self, email, password, user_role, id, active=True):
        self.email = email
        self.password = password
        self.user_role = user_role
        self.id = id
        self.active = active

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.email)