import hashlib, MySQLdb, telnetlib, paramiko, requests, cchardet

from web.modules.Pinger import Pinger
from cfg import MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB, DOMAIN, SECRET_KEY
from flask import Flask, render_template, request, redirect, url_for
from flask.ext.login import LoginManager, login_required, current_user, login_user, logout_user
from web.modules.users.model import User
from threading import Thread

# basic setup
app = Flask(__name__)
app.secret_key = SECRET_KEY
app.debug = True

# setup flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "/"

MAILGUN_KEY = 'key-c030377da931a5ea70a1dc11b3a854af'
MAILGUN_API = 'https://api.mailgun.net/v3'
MAILGUN_DOMAIN = 'sandbox536ca892cea54d3aad3815ff2e7e7e6c.mailgun.org'


@app.route('/', methods=['GET', 'POST'])
def index():
    if current_user.is_authenticated():
        return redirect(url_for("home"))

    if request.method == 'POST':
        next_url = request.args.get("next", "/home")
        email = request.form.get('email', None)
        passwd = request.form.get('password', None)
        sign = create_signature(convert_encoding(passwd))

        if email is not None:
            user = get_user(email, sign)
            if user is not None:
                if login_user(user):
                    return redirect(next_url)
            else:
                print "User not found"

    return render_template("login.html", **{

    })


@app.route('/home', methods=['GET'])
@login_required
def home():
    hosts = []
    ap = get_ap()
    for i in ap:
        hosts.append(i.ip)

    ping = Pinger()
    ping.thread_count = 3
    ping.hosts = hosts
    status = ping.start()
    #dead = uniqify(status['dead'])
    #alive = uniqify(status['alive'])

    for i in ap:
        if i.ip in status['alive']:
            i.status = True
        else:
            db, cur = mysql_connect()
            try:
                cur.execute("insert into ap_down (ap_id) values (%s)", (i.id, ))
                db.commit()
            except MySQLdb.Error:
                db.rollback()
            db.close()

            subject = "Host " + i.ip + " is down"
            msg = """\
                Hi Admin,<br /><br />
                System report that your Access Point <strong>(""" + i.name + """)</strong> is currently down.<br /><br />

                Regards,<br />
                WICO
            """
            i.status = False
            t = Thread(target=_send_mailgun, args=(msg, subject, current_user.email, "no-reply@wico.com", 'Wico Alert'))
            t.setDaemon(True)
            t.start()

    return render_template("home.html", **{
        'ap': ap
    })


@app.route('/ap/list', methods=['GET'])
@login_required
def list_ap():
    ap = get_ap()

    return render_template("ap_list.html", **{
        'ap': ap
    })


@app.route('/ap/add', methods=['GET', 'POST'])
@login_required
def add_ap():
    success = False

    if request.method == 'POST':
        name = request.form.get("ap_name")
        ip = request.form.get("ip_address")
        username = request.form.get("username")
        passwd = request.form.get("password")
        _os = request.form.get("os")

        db, cur = mysql_connect()
        try:
            cur.execute("insert into ap (name, ip_address, username, password, os) values (%s, %s, %s, %s, %s)", (name, ip, username, passwd, _os))
            db.commit()
            success = True
        except MySQLdb.Error:
            db.rollback()
        db.close()

    return render_template("ap_add.html", **{
        'success': success
    })


@app.route('/ap/edit/<id>', methods=['GET', 'POST'])
@login_required
def edit_ap(id):
    success = False

    if request.method == 'POST':
        name = request.form.get("ap_name")
        ip = request.form.get("ip_address")
        username = request.form.get("username")
        passwd = request.form.get("password")
        _os = request.form.get("os")

        db, cur = mysql_connect()
        try:
            cur.execute("update ap set name = %s, ip_address = %s, username = %s, password = %s, os = %s where id = %s", (name, ip, username, passwd, _os, int(id)))
            db.commit()
            success = True
        except MySQLdb.Error:
            db.rollback()
        db.close()

    _ap = None
    ap = get_ap()

    for i in ap:
        if i.id == int(id):
            _ap = i

    return render_template("ap_edit.html", **{
        'ap': _ap,
        'success': success
    })


@app.route('/ap/log/<id>', methods=['GET'])
@login_required
def log_ap(id):
    _ap = None
    ap = get_ap()

    for i in ap:
        if i.id == int(id):
            _ap = i

    if _ap.os == 1:
        ## TP-Link TD-W8901N
        log = ["Log unavailable for this access point"]
    elif _ap.os == 2:
        ## OpenWRT
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(_ap.ip, username=_ap.username, password=_ap.passwd)
        stdin, stdout, stderr = ssh.exec_command('logread')
        log = stdout.readlines()
        ssh.close()
    elif _ap.os == 3:
        ## DSL-2750E
        log = ["Log unavailable for this access point"]

    return render_template("ap_log.html", **{
        'log': log
    })


@app.route('/ap/delete/<id>', methods=['GET'])
@login_required
def delete_ap(id):
    db, cur = mysql_connect()
    try:
        cur.execute("delete from ap where id = %s", (id, ))
        db.commit()
    except MySQLdb.Error:
        db.rollback()
    db.close()

    return redirect(url_for("list_ap"))


@app.route('/ap/reboot/<id>', methods=['GET'])
@login_required
def reboot_ap(id):
    _ap = None
    ap = get_ap()

    for i in ap:
        if i.id == int(id):
            _ap = i

    if _ap.os == 1:
        ## TP-Link TD-W8901N
        tn = telnetlib.Telnet(_ap.ip)
        tn.read_until("Password:")
        tn.write(_ap.passwd + "\n")
        tn.read_until('TP-LINK>')
        tn.write("sys reboot\n")
        tn.close()
    elif _ap.os == 2:
        ## OpenWRT
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(_ap.ip, username=_ap.username, password=_ap.passwd)
        stdin, stdout, stderr = ssh.exec_command('reboot')
        ssh.close()
    elif _ap.os == 3:
        ## DSL-2750E
        tn = telnetlib.Telnet(_ap.ip)
        tn.set_debuglevel(100)
        tn.read_until("username:")
        tn.write(_ap.username + "\n")
        tn.read_until("password:")
        tn.write(_ap.passwd + "\n")
        tn.read_until('TBS>>')
        tn.write("reboot\n")
        tn.close()

    db, cur = mysql_connect()
    try:
        msg = "Reboot"
        cur.execute("insert into logs (user_id, ap_id, msg) values (%s, %s, %s)", (current_user.id, _ap.id, msg))
        db.commit()
    except MySQLdb.Error:
        db.rollback()
    db.close()

    return redirect(url_for("list_ap"))


@app.route('/report', methods=['GET'])
@login_required
def report():
    downtime = get_downtime()

    period = {}
    for i in downtime:
        period[i.dt_down.strftime('%Y-%m-%d %H:%M')] = {}
        for x in downtime:
            if i.dt_down == x.dt_down:
                period[i.dt_down.strftime('%Y-%m-%d %H:%M')].update({"period": x.dt_down.strftime('%Y-%m-%d %H:%M'), x.name: x.total})

    data = ""
    ap_name = []
    for i in period:
        data += "{"
        data += "period: '" + i + "',"
        for x in period[i]:
            if x != "period":
                data += "'" + x + "'" + ": " + str(period[i][x]) + ","
                ap_name.append(x)
        data = data[:-1]
        data += "},"
    data = data[:-1]

    ap_name_str = ""
    for i in uniqify(ap_name):
        ap_name_str += "'" + i + "',"
    ap_name_str = ap_name_str[:-1]

    logs = get_logs()

    reboot_data = ""
    reboot = get_reboot_count()
    for i in reboot:
        reboot_data += "{label: '" + i.name + "', value: " + str(i.total) + "},"
    reboot_data = reboot_data[:-1]

    return render_template("report.html", **{
        'downtime': data,
        'ap_name': ap_name_str,
        'history': downtime,
        'reboot_logs': logs,
        'reboot_data': reboot_data
    })


def logout():
    logout_user()
    return redirect("/")


@login_manager.user_loader
def load_user(id):
    db, cur = mysql_connect()
    cur.execute("SELECT * FROM users WHERE id = %s", (id, ))
    user = cur.fetchone()
    db.close()

    try:
        user = User(user['email'], user['password'], user['user_role'], int(user['id']))
    except MySQLdb.Error:
        user = None
    except TypeError:
        user = None

    return user


def get_user(email, password):
    db, cur = mysql_connect()
    cur.execute("SELECT * FROM users WHERE email = %s AND password = %s", (email, password, ))
    user = cur.fetchone()
    db.close()

    try:
        user = User(user['email'], user['password'], user['user_role'], int(user['id']))
    except MySQLdb.Error:
        user = None
    except TypeError:
        user = None

    return user


def get_ap():
    ap = []
    db, cur = mysql_connect()
    cur.execute("SELECT * FROM ap")
    db.close()

    for row in cur.fetchall():
        i = Object()
        i.id = row['id']
        i.name = row['name']
        i.ip = row['ip_address']
        i.username = row['username']
        i.passwd = row['password']
        i.os = row['os']
        ap.append(i)

    return ap


def get_downtime():
    downtime = []
    db, cur = mysql_connect()
    sql = """
        select ap.name, ad.created as dt_down, count(ap.name) as total
        from ap_down ad, ap
        where ap.id = ad.ap_id
        group by minute(dt_down), ap.name
        order by ap.name;
    """
    cur.execute(sql)
    db.close()

    for row in cur.fetchall():
        i = Object()
        i.name = row['name']
        i.dt_down = row['dt_down']
        i.total = row['total']
        downtime.append(i)

    return downtime


def get_logs():
    logs = []
    db, cur = mysql_connect()
    sql = """
        select u.email, ap.name, l.msg, l.created
        from users u, ap, logs l
        where l.user_id = u.id
        and l.ap_id = ap.id
        order by created desc;
    """
    cur.execute(sql)
    db.close()

    for row in cur.fetchall():
        i = Object()
        i.msg = row['name'] + " was reboot by " + row['email'] + " at " + row['created'].strftime('%Y-%m-%d %H:%M:%S')
        logs.append(i)

    return logs


def get_reboot_count():
    reboot = []
    db, cur = mysql_connect()
    sql = """
        select ap.name, count(ap.name) as total
        from ap, logs l
        where l.ap_id = ap.id
        group by ap.name
        order by ap.name;
    """
    cur.execute(sql)
    db.close()

    for row in cur.fetchall():
        i = Object()
        i.name = row['name']
        i.total = row['total']
        reboot.append(i)

    return reboot


def create_signature(data):
    return hashlib.sha1(repr(data) + "," + SECRET_KEY).hexdigest()


def verify_signature(data, signature):
    return signature == create_signature(data)


def mysql_connect():
    db = MySQLdb.connect(host=MYSQL_HOST, user=MYSQL_USER, passwd=MYSQL_PASS, db=MYSQL_DB)
    cur = db.cursor(MySQLdb.cursors.DictCursor)
    return db, cur


def get_ap_os(id):
    return {
        '1': 'TD-W8901N',
        '2': 'OpenWRT',
        '3': 'DSL-2750E'
    }.get(id, '1')


def uniqify(seq, idfun=None):
   # order preserving
   if idfun is None:
       def idfun(x): return x
   seen = {}
   result = []
   for item in seq:
       marker = idfun(item)
       # in old Python versions:
       # if seen.has_key(marker)
       # but in new ones:
       if marker in seen: continue
       seen[marker] = 1
       result.append(item)

   return result


def _send_mailgun(msg, email_subject, email_to, email_from, email_from_name):
    message = {
        'from': "%s <%s>" % (email_from_name, email_from),
        'to': email_to,
        'subject': email_subject,
        'html': msg,
    }

    result = requests.post('%s/%s/messages' % (MAILGUN_API, MAILGUN_DOMAIN), auth=('api', MAILGUN_KEY), data=message)
    print(result.json())
    return


def convert_encoding(data, new_coding='UTF-8'):
    encoding = cchardet.detect(data)['encoding']

    if new_coding.upper() != encoding.upper():
        data = data.decode(encoding, data).encode(new_coding)

    return data


app.add_url_rule('/logout', "logout_user", logout, methods=['GET'])


class Object(object):
    pass