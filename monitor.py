import hashlib, MySQLdb, requests

from web.modules.Pinger import Pinger
from cfg import MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB, SECRET_KEY
from manager import Manager


MAILGUN_KEY = 'key-c030377da931a5ea70a1dc11b3a854af'
MAILGUN_API = 'https://api.mailgun.net/v3'
MAILGUN_DOMAIN = 'sandbox536ca892cea54d3aad3815ff2e7e7e6c.mailgun.org'
manager = Manager()


@manager.command
def ap():
    """
    Get all AP status
    """

    hosts = []
    _ap = get_ap()
    for i in _ap:
        hosts.append(i.ip)

    ping = Pinger()
    ping.thread_count = 3
    ping.hosts = hosts
    status = ping.start()

    for i in _ap:
        if i.ip in status['alive']:
            i.status = True
        else:
            subject = "Host " + i.ip + " is down"
            msg = """\
                Hi Admin,<br /><br />
                System report that your Access Point <strong>(""" + i.name + """)</strong> is currently down.<br /><br />

                Regards,<br />
                WICO
            """
            i.status = False
            _send_mailgun(msg, subject, "wicofyp@gmail.com", "no-reply@wico.com", "Wico Alert")

@manager.command
def test():
    print create_signature("Password")


@manager.command
def create_user(email, passwd):
    """
    Create user for wico [email, password]
    """

    db, cur = mysql_connect()
    try:
        passwd = create_signature(passwd)
        cur.execute("insert into users (email, password, user_role, is_active) values (%s, %s, %s, %s)", (email, passwd, 1, 1))
        db.commit()
    except MySQLdb.Error:
        db.rollback()
    db.close()
    print "User " + email + " created."


def mysql_connect():
    db = MySQLdb.connect(host=MYSQL_HOST, user=MYSQL_USER, passwd=MYSQL_PASS, db=MYSQL_DB)
    cur = db.cursor(MySQLdb.cursors.DictCursor)
    return db, cur


def get_ap():
    ap = []
    db, cur = mysql_connect()
    cur.execute("SELECT * FROM ap")
    db.close()

    for row in cur.fetchall():
        i = Object()
        i.id = row['id']
        i.name = row['name']
        i.ip = row['ip_address']
        i.username = row['username']
        i.passwd = row['password']
        i.os = row['os']
        ap.append(i)

    return ap


def _send_mailgun(msg, email_subject, email_to, email_from, email_from_name):
    message = {
        'from': "%s <%s>" % (email_from_name, email_from),
        'to': email_to,
        'subject': email_subject,
        'html': msg,
    }

    result = requests.post('%s/%s/messages' % (MAILGUN_API, MAILGUN_DOMAIN), auth=('api', MAILGUN_KEY), data=message)
    print(result.json())
    return


def create_signature(data):
    return hashlib.sha1(repr(data) + "," + SECRET_KEY).hexdigest()


class Object(object):
    pass


if __name__ == '__main__':
    manager.main()


